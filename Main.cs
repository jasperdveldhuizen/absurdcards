﻿using BepInEx;
using UnboundLib;
using UnboundLib.Cards;
using HarmonyLib;
using UnityEngine;
using AbsurdCards.Cards;

namespace AbsurdCards
{
    [BepInDependency("com.willis.rounds.unbound", BepInDependency.DependencyFlags.HardDependency)]
    [BepInDependency("pykess.rounds.plugins.moddingutils", BepInDependency.DependencyFlags.HardDependency)]
    [BepInPlugin("org.bepinex.plugins.AbsurdCards", ModName, "1.0.0.0")]
    [BepInProcess("Rounds.exe")]
    public class AbsurdCards : BaseUnityPlugin
    {
        private const string ModName = "AbsurdCards mod";

        // Awake is called once when both the game and the plug-in are loaded
        void Awake()
        {
            new Harmony(ModName).PatchAll();
        }

        private void Start()
        {
            Unbound.RegisterCredits(ModName, new string[] { "Jasper", "Alex" }, "", "");

            UnityEngine.Debug.Log("Loaded AbsurdCards mod!");

            CustomCard.BuildCard<Cannon>();
            CustomCard.BuildCard<Barrier>();
        }
    }
}
