﻿using Photon.Pun;
using ModdingUtils.RoundsEffects;
using UnboundLib;
using UnityEngine;
using AbsurdCards.Utils;
using UnityEngine.Events;

namespace AbsurdCards.Events
{
    public class SpawnBoxEvent : HitSurfaceEffect
    {
        public TimeSince timeSinceLastBox;

        private CharacterStatModifiers stats;

        public void Awake()
        {
            if (GetComponent<CharacterStatModifiers>())
            {
                stats = GetComponent<CharacterStatModifiers>();
            }
        }

        public override void Hit(Vector2 position, Vector2 normal, Vector2 velocity)
        {
            if (timeSinceLastBox > 1f)
            {
                timeSinceLastBox = 0;
                if (PhotonNetwork.IsMasterClient)
                {
                    for (int i = 0; i < 5; i++)
                    {
                        this.ExecuteAfterSeconds(0.20f * i, () =>
                        {
                            PhotonNetwork.Instantiate("4 map objects/Box", (position + (0.75f + i * 0.3f) * normal), Quaternion.identity);

                            this.ExecuteAfterSeconds(0.08f, () =>
                            {
                                GetComponent<PhotonView>().RPC("RPCA_FixBox", RpcTarget.All);
                            });
                            this.ExecuteAfterSeconds(0.15f, () =>
                            {
                                GetComponent<PhotonView>().RPC("RPCA_FixBox", RpcTarget.All);
                            });
                        });
                    }
                }
            }
        }

        [PunRPC]
        public void RPCA_FixBox()
        {
            var currentObj = MapManager.instance.currentMap.Map.gameObject.transform.GetChild(MapManager.instance.currentMap.Map.gameObject.transform.childCount - 1);
            var rigid = currentObj.GetComponent<Rigidbody2D>();
            rigid.isKinematic = false;
            rigid.simulated = true;
            //currentObj.GetComponent<DamagableEvent>().deathEvent = new UnityEvent();
            //currentObj.GetComponent<DamagableEvent>().deathEvent.AddListener(() =>
            //{
            //     Destroy(currentObj.gameObject);
            //});
        }
    }
}
